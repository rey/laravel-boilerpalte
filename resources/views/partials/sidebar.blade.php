<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                    <img alt="{{Auth::user()->first_name}}" class="img-circle" src="https://www.gravatar.com/avatar/{{md5(Auth::user()->email)}}?d=mm&s=48" />
                     </span>
                    <a data-toggle="dropdown" class="dropdown-toggle">
                        @if(Auth::check())
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->first_name ." ". Auth::user()->last_name }}</strong>
                        @else
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">User</strong>

                        @endif
                     </span> <span class="text-muted text-xs block">admin<b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a>{{trans('PartnersAbs::sidebar.profile')}}</a></li>
                        <li class="divider"></li>
                        <li><a href="{{route('api.auth.logout')}}">{{trans('PartnersAbs::sidebar.logout')}}</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                </div>
            </li>
            @include('PartnersAbs::partials.sidebar_items')
        </ul>
    </div>
</nav>
