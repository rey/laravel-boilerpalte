<ul class="nav metismenu">
    @foreach ($menuItems as $itemIndex => $item)
        <li class="@if(isset($item['url'])) @if(Request::url() === route($item['url'])) {{"active"}} @endif @endif" id="">
            @if (empty($item['submenu']))
                <a href="@if (isset($item['url'])) {{route($item['url'])}} @endif">
                    <i class="fa fa-{{$item['icon'] or ''}}"></i>
                    <span class="nav-label">{{  trans($itemIndex) }}</span>
                </a>
            @else
                @foreach($permissions as $index => $permission)
                    @if(hasAnyPermission($index,$item['items']))
                        <a href="@if (isset($item['url'])) {{route($item['url'])}} @endif">
                            <i class="fa fa-{{$item['icon'] or ''}}"></i>
                            <span class="nav-label">{{ trans($itemIndex) }}</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            @foreach ($item['submenu'] as $subItemIndex => $subitem)
                                @foreach($permissions as $in => $permission)
                                    @if(hasPermission($in,$subitem['slug']))
                                    <li class="@if(isset($subitem['url'])) @if(Request::url() === route($subitem['url'])) {{"active"}} @endif @endif">
                                        <a href="@if (isset($subitem['url'])) {{route($subitem['url'])}} @endif">
                                            <i class="fa fa-{{$subitem['icon'] or ''}}"></i>
                                            <span class="nav-label">
                                            {{  trans($subItemIndex) }}
                                            </span>
                                        </a>
                                    </li>
                                    @endif
                                @endforeach
                            @endforeach
                        </ul>
                    @endif
                @endforeach
            @endif
        </li>
    @endforeach
</ul>