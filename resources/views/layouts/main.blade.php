<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title_box', 'Principal') || DonParlay.com #BOX+</title>
    {!! Html::style('assets/admin/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('assets/admin/css/bootstrap.min.css') !!}
    {!! Html::style('assets/admin/css/style.css') !!}
    {!! Html::style('assets/admin/css/animate.css') !!}
    {!! Html::style('assets/admin/css/plugins/toastr/toastr.min.css') !!}
</head>
<body class="">
<div id="wrapper">
    @include('PartnersAbs::partials.sidebar')
    <div id="page-wrapper" class="gray-bg">
        @include('PartnersAbs::partials.topbar')

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-9">
                <h2>
                    @yield('title_page')
                </h2>
                @yield('breadcrumbs')
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>@yield('ibox_title')</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            @include('PartnersAbs::partials.errors')
                            @include('PartnersAbs::partials.success')
                            @yield('ibox_content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Mainly scripts -->
{!! Html::script('assets/admin/js/jquery-2.1.1.js') !!}
{!! Html::script('assets/admin/js/bootstrap.min.js') !!}
{!! Html::script('assets/admin/js/plugins/metisMenu/jquery.metisMenu.js') !!}
{!! Html::script('assets/admin/js/plugins/slimscroll/jquery.slimscroll.min.js') !!}
<!-- Custom and plugin javascript -->
{!! Html::script('assets/admin/js/app.js') !!}
{!! Html::script('assets/admin/js/plugins/pace/pace.min.js') !!}}
<!-- jQuery UI -->
{!! Html::script('assets/admin/js/plugins/jquery-ui/jquery-ui.min.js') !!}
<!-- Toastr -->
{!! Html::script('assets/admin/js/plugins/toastr/toastr.min.js') !!}

@yield('scripts')
</body>
</html>