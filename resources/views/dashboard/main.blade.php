@extends('PartnersAbs::layouts.main')
@section('title_box','Dashboard')
@endsection
@section('title_page','DashBoard')
@endsection
@section('breadcrumbs', Breadcrumbs::render('dashboard'))
@endsection
@section('ibox_title')
    Titulo del ibox
@endsection
@section('ibox_content')
    <div class="wrapper wrapper-content">
        <div class="middle-box text-center animated fadeInRightBig">
            <h3 class="font-bold">This is page content</h3>
            <div class="error-desc">
                You can create here any grid layout you want. And any variation layout you imagine:) Check out
                main dashboard and other site. It use many different layout.
                <br/><a href="#" class="btn btn-primary m-t">{{trans('PartnersAbs::menu.dashboard')}}</a>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection